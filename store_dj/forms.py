from django import forms
from django.contrib.auth.models import User


#formularios con django
class RegisterForm(forms.Form):
    usuario = forms.CharField(required=True, min_length=4,max_length=50, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'id': 'usuario',
        'placeholder':'Ingrese el nombre de usuario'
    }))
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'id': 'email',
        'placeholder':'example@dominio.com'
    }))
    contraseña = forms.CharField(required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'id': 'password',
        'placeholder':'Ingrese la contraseña de usuario'
    }))
    con_contraseña = forms.CharField(label='confirmar contraseña',required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'id': 'password',
        'placeholder':'Confirme la contraseña de usuario'
    }))

    def clean_usuario(self):
        usuario = self.cleaned_data.get('usuario')
        if User.objects.filter(username=usuario).exists():
            raise forms.ValidationError('El usuario ya esta en uso')
        return usuario

    def clean_email(self):
        #Obtener la informacion del campo
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('El E-Mail ya esta en uso')
        return email

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('con_contraseña') != cleaned_data.get('contraseña'):
            self.add_error('con_contraseña','Las contraseñas no coinciden')

    def save(self):
        return User.objects.create_user(
            username = self.cleaned_data.get('usuario'),
            email = self.cleaned_data.get('email'),
            password = self.cleaned_data.get('contraseña')
        )


from django.contrib import admin
from django.urls import path, re_path
from . import views
from products.views import ProductListView

urlpatterns = [
    path('',ProductListView.as_view(),name = 'index'),
    path('users/login',views.login_view,name = 'login'),
    path('users/logout',views.logout_view,name = 'logout'),
    path('users/register',views.register,name = 'register'),
    path('admin/', admin.site.urls),
]
